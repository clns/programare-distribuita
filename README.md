# Programare Distribuita (Java)

Implementari ale [numerelor lui Bernoulli](http://en.wikipedia.org/wiki/Bernoulli_number) folosind tehnologii distribuite in Java. Sistemul de operare folosit este OS X 10.9.3.

### Tehnologii

* [ServerSocket (TCP)](https://bitbucket.org/calinseciu/programare-distribuita/src/HEAD/src/tcp/?at=master)
* [ServerSocketChannel](https://bitbucket.org/calinseciu/programare-distribuita/src/HEAD/src/serverchannel/?at=master)
* [DatagramSocket (UDP)](https://bitbucket.org/calinseciu/programare-distribuita/src/HEAD/src/udp/?at=master)
* [RMI](https://bitbucket.org/calinseciu/programare-distribuita/src/HEAD/src/rmi/?at=master)
* [Servlet](https://bitbucket.org/calinseciu/programare-distribuita/src/HEAD/src/servlet/?at=master)
* [Servlet RMI](https://bitbucket.org/calinseciu/programare-distribuita/src/HEAD/src/servletrmi/?at=master)
* [Applet](https://bitbucket.org/calinseciu/programare-distribuita/src/HEAD/src/applet/?at=master)
* [JSP](https://bitbucket.org/calinseciu/programare-distribuita/src/HEAD/src/jsp/?at=master)

### Structura fisiere

* `bin/` contine script-urile pentru a rula programele server-client utilizand ant, precum si alte resurse necesare (ex. apache-tomcat); am creat aceste script-uri pentru a fi mai usor sa rulez programele
* `src/data/` contine clasa Bernoulli care genereaza numerele in functie de parametrul n, precum si o clasa Rational pentru a lucra cu numere rationale
* `src/[tehnologie-java]/src/` contine toate sursele java necesare
* `src/[tehnologie-java]/*.xml` fisierele ant
* `src/[tehnologie-java]/[altceva]` fisiere generate in urma compilarii cu ant; nu sunt necesare pentru executie

### Dependente

* [ant](http://ant.apache.org/) - Comanda `$ ant` trebuie sa fie accesibila in consola
* [apache-tomcat](http://tomcat.apache.org/) - Se presupune ca se afla la _~/Downloads/apache-tomcat-8.0.8_. Se porneste cu [bin/apachetomcat-start.sh](https://bitbucket.org/calinseciu/programare-distribuita/src/HEAD/bin/apachetomcat-start.sh?at=master)
* [apache-ftpserver](http://mina.apache.org/ftpserver-project/) - Se presupune ca se afla la _~/Downloads/apache-ftpserver-1.0.6_. Se porneste cu [bin/ftpserver.sh](https://bitbucket.org/calinseciu/programare-distribuita/src/HEAD/bin/ftpserver.sh?at=master)
* [apache-httpd](https://httpd.apache.org/) - Deoarece Apache HTTP este preinstalat pe OS X, unele script-uri folosesc locatia _/Library/WebServer/Documents/progr-distr/_ care reprezinta DOCUMENT_ROOT pentru a face accesibile resursele la http://localhost/progr-distr/. Se putea insa folosi doar apache-tomcat.
