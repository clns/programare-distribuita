## RMI

### Exemplu Executie

#### Server

* Server FTP

```
Calins-MacBook-Pro:Bernoulli calin$ bin/ftpserver.sh
Using XML configuration file res/conf/ftpd-typical.xml...
FtpServer started
```

* rmiregistry

```
Calins-MacBook-Pro:Bernoulli calin$ bin/rmi-registry.sh
Buildfile: /Users/calin/IdeaProjects/Bernoulli/src/rmi/server.xml

Rmi:
```

Aici a fost o problema. Trebuie inclus argumentul __java.rmi.server.useCodebaseOnly=false__ in fisierul ant. Asta mi-a luat foarte mult timp pana am descoperit de ce nu mergea.

* Server

```
Calins-MacBook-Pro:Bernoulli calin$ bin/rmi-server.sh
Buildfile: /Users/calin/IdeaProjects/Bernoulli/src/rmi/interface.xml

Init:
   [delete] Deleting directory /Users/calin/IdeaProjects/Bernoulli/src/rmi/public
    [mkdir] Created dir: /Users/calin/IdeaProjects/Bernoulli/src/rmi/public

Compile:
    [javac] Compiling 1 source file to /Users/calin/IdeaProjects/Bernoulli/src/rmi/public
      [jar] Building jar: /Users/calin/IdeaProjects/Bernoulli/src/rmi/public/bernoulli.jar

BUILD SUCCESSFUL
Total time: 1 second
Buildfile: /Users/calin/IdeaProjects/Bernoulli/src/rmi/server.xml

Init:
   [delete] Deleting directory /Users/calin/IdeaProjects/Bernoulli/src/rmi/server
    [mkdir] Created dir: /Users/calin/IdeaProjects/Bernoulli/src/rmi/server

Compile:
    [javac] Compiling 1 source file to /Users/calin/IdeaProjects/Bernoulli/src/rmi/server
    [unjar] Expanding: /Users/calin/IdeaProjects/Bernoulli/src/rmi/public/bernoulli.jar into /Users/calin/IdeaProjects/Bernoulli/src/rmi/server

Archive:
      [jar] Building jar: /Users/calin/Downloads/apache-ftpserver-1.0.6/res/home/rmi/bernoulli.jar

Server:
     [java] Bernoulli RMI server ready ...
```

#### Client

* n=4

```
Calins-MacBook-Pro:Bernoulli calin$ bin/rmi-client.sh
Buildfile: /Users/calin/IdeaProjects/Bernoulli/src/rmi/client.xml

Init:
   [delete] Deleting directory /Users/calin/IdeaProjects/Bernoulli/src/rmi/client
    [mkdir] Created dir: /Users/calin/IdeaProjects/Bernoulli/src/rmi/client

Compile:
    [javac] Compiling 1 source file to /Users/calin/IdeaProjects/Bernoulli/src/rmi/client

Run:
4
     [java] B(0) = 1
     [java] B(1) = -1/2
     [java] B(2) = 1/6
     [java] B(3) = 0

BUILD SUCCESSFUL
```