package rmi.src;

import data.Bernoulli;

import java.rmi.server.UnicastRemoteObject;
// Call rmiregistry directly
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
// Using JNDI
//import javax.naming.Context;
//import javax.naming.InitialContext;

public class Server implements Interface {

    public Bernoulli bernoulli(int n) {
        return new Bernoulli(n);
    }

    public static void main(String args[]) {
        String host = "localhost";
        int port = 1099;

        try {
            Server obj = new Server();
            Interface stub = (Interface)UnicastRemoteObject.exportObject(obj, 0);

            // Call rmiregistry directly
            Registry registry = LocateRegistry.getRegistry(host, port);
            registry.bind("Bernoulli", stub);

            // Using JNDI
//            String sPort=(new Integer(port)).toString();
//            System.setProperty(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.rmi.registry.RegistryContextFactory");
//            System.setProperty(Context.PROVIDER_URL, "rmi://"+host+":"+sPort);
//            Context ctx = new InitialContext();
//            ctx.bind("Bernoulli", stub);

            System.out.println("Bernoulli RMI server ready ...");
        }
        catch (Exception e) {
            System.err.println("Error in RMI Server: " + e.getMessage());
        }
    }
}
