package rmi.src;

import data.Bernoulli;

import java.util.Scanner;
// Call rmiregistry directly
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
// Using JNDI
//import javax.naming.Context;
//import javax.naming.InitialContext;

public class Client {

    public static void main(String args[]) {
        String host = "localhost";
        int port = 7999;
        if (args.length > 0) {
            host = args[0];
        }
        if (args.length > 1) {
            port = Integer.parseInt(args[1]);
        }

        Scanner scanner = new Scanner(System.in);
//        System.out.print("Introduceti n: ");
        int n = scanner.nextInt();

        try {
            // Call rmiregistry directly
            Registry registry = LocateRegistry.getRegistry(host, port);
            Interface obj = (Interface)registry.lookup("Bernoulli");

            // Using JNDI
//            String sPort=(new Integer(port)).toString();
//            System.setProperty(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.rmi.registry.RegistryContextFactory");
//            System.setProperty(Context.PROVIDER_URL, "rmi://"+host+":"+sPort);
//            Context ctx = new InitialContext();
//            Interface obj = (Interface)ctx.lookup("Bernoulli");

            Bernoulli bernoulli = obj.bernoulli(n);
            bernoulli.print();
        }
        catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}
