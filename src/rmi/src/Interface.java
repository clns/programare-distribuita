package rmi.src;

import data.Bernoulli;

public interface Interface extends java.rmi.Remote {
    Bernoulli bernoulli(int n) throws java.rmi.RemoteException;
}
