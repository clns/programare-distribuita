<jsp:useBean id="obj" class="jsp.src.JSP" scope="application"/>
<jsp:setProperty name="obj" property="*"/>
<%@ page import = "java.util.*"%>
<%@ page import = "java.lang.*"%>
<!DOCTYPE HTML>
<html>
<head>
    <title>Bernoulli JSP</title>
</head>
<body>
<h1>Bernoulli JSP</h1>
<%
    String sn = request.getParameter("n");
    int n = Integer.parseInt(sn);
    obj.setN(n);
%>
<p>
    Bernoulli pentru N=<% out.println(sn); %> este:<br>
    <%=obj.getBernoulliAsHtml()%>
</p>
</body>
</html>