## JSP

### Exemplu Executie

#### Server

* Pornire apache-tomcat

```
Calins-MacBook-Pro:Bernoulli calin$ bin/apachetomcat-start.sh
Using CATALINA_BASE:   /Users/calin/Downloads/apache-tomcat-8.0.8
Using CATALINA_HOME:   /Users/calin/Downloads/apache-tomcat-8.0.8
Using CATALINA_TMPDIR: /Users/calin/Downloads/apache-tomcat-8.0.8/temp
Using JRE_HOME:        /Library/Java/JavaVirtualMachines/jdk1.7.0_45.jdk/Contents/Home
Using CLASSPATH:       /Users/calin/Downloads/apache-tomcat-8.0.8/bin/bootstrap.jar:/Users/calin/Downloads/apache-tomcat-8.0.8/bin/tomcat-juli.jar
Tomcat started.
```

* Generare JSP

```
Calins-MacBook-Pro:Bernoulli calin$ bin/jsp.sh
Buildfile: /Users/calin/IdeaProjects/Bernoulli/src/jsp/build.xml

Init:
   [delete] Deleting directory /Users/calin/IdeaProjects/Bernoulli/src/jsp/web/WEB-INF/classes
    [mkdir] Created dir: /Users/calin/IdeaProjects/Bernoulli/src/jsp/web/WEB-INF/classes
   [delete] Deleting directory /Users/calin/Downloads/apache-tomcat-8.0.8/webapps/bernoulli-jsp
    [mkdir] Created dir: /Users/calin/Downloads/apache-tomcat-8.0.8/webapps/bernoulli-jsp

Compile:
    [javac] Compiling 1 source file to /Users/calin/IdeaProjects/Bernoulli/src/jsp/web/WEB-INF/classes

Deploy:
     [copy] Copying 5 files to /Users/calin/Downloads/apache-tomcat-8.0.8/webapps/bernoulli-jsp

BUILD SUCCESSFUL
```

#### Client

Se acceseaza http://localhost:8080/bernoulli-jsp/ in browser.

![jsp-1.png](https://bitbucket.org/repo/oAjoMy/images/207920046-jsp-1.png)

![jsp-2.png](https://bitbucket.org/repo/oAjoMy/images/3816365670-jsp-2.png)