package jsp.src;

import data.Bernoulli;

public class JSP {
    private int n;

    public void setN(int n) {
        this.n = n;
    }

    public String getBernoulliAsHtml() {
        Bernoulli bernoulli = new Bernoulli(this.n);
        return bernoulli.asHtml();
    }
}