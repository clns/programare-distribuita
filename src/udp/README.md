## DatagramSocket (UDP)

### Exemplu Executie

#### Server

```
Calins-MacBook-Pro:Bernoulli calin$ bin/udp-server.sh
Buildfile: /Users/calin/IdeaProjects/Bernoulli/src/udp/server.xml

Init:
   [delete] Deleting directory /Users/calin/IdeaProjects/Bernoulli/src/udp/server
    [mkdir] Created dir: /Users/calin/IdeaProjects/Bernoulli/src/udp/server

Compile:
    [javac] Compiling 1 source file to /Users/calin/IdeaProjects/Bernoulli/src/udp/server

Run:
     [java] DatagramSocket is ready ...
```

#### Client

* n=3

```
Calins-MacBook-Pro:Bernoulli calin$ bin/udp-client.sh
Buildfile: /Users/calin/IdeaProjects/Bernoulli/src/udp/client.xml

Init:
   [delete] Deleting directory /Users/calin/IdeaProjects/Bernoulli/src/udp/client
    [mkdir] Created dir: /Users/calin/IdeaProjects/Bernoulli/src/udp/client

Compile:
    [javac] Compiling 1 source file to /Users/calin/IdeaProjects/Bernoulli/src/udp/client

Run:
3
     [java] B(0) = 1
     [java] B(1) = -1/2
     [java] B(2) = 1/6

BUILD SUCCESSFUL
```

* n=6

```
Calins-MacBook-Pro:Bernoulli calin$ bin/udp-client.sh
Buildfile: /Users/calin/IdeaProjects/Bernoulli/src/udp/client.xml

Init:
   [delete] Deleting directory /Users/calin/IdeaProjects/Bernoulli/src/udp/client
    [mkdir] Created dir: /Users/calin/IdeaProjects/Bernoulli/src/udp/client

Compile:
    [javac] Compiling 1 source file to /Users/calin/IdeaProjects/Bernoulli/src/udp/client

Run:
6
     [java] B(0) = 1
     [java] B(1) = -1/2
     [java] B(2) = 1/6
     [java] B(3) = 0
     [java] B(4) = -1/30
     [java] B(5) = 0

BUILD SUCCESSFUL
```