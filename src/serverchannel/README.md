## ServerSocketChannel

### Exemplu Executie

#### Server

```
Calins-MacBook-Pro:Bernoulli calin$ bin/serverchannel-server.sh
Buildfile: /Users/calin/IdeaProjects/Bernoulli/src/serverchannel/server.xml

Init:
   [delete] Deleting directory /Users/calin/IdeaProjects/Bernoulli/src/serverchannel/server
    [mkdir] Created dir: /Users/calin/IdeaProjects/Bernoulli/src/serverchannel/server

Compile:
    [javac] Compiling 1 source file to /Users/calin/IdeaProjects/Bernoulli/src/serverchannel/server

Run:
     [java] Server ready...
```

#### Client

* n=4

```
Calins-MacBook-Pro:Bernoulli calin$ bin/serverchannel-client.sh
Buildfile: /Users/calin/IdeaProjects/Bernoulli/src/serverchannel/client.xml

Init:
   [delete] Deleting directory /Users/calin/IdeaProjects/Bernoulli/src/serverchannel/client
    [mkdir] Created dir: /Users/calin/IdeaProjects/Bernoulli/src/serverchannel/client

Compile:
    [javac] Compiling 1 source file to /Users/calin/IdeaProjects/Bernoulli/src/serverchannel/client

Run:
4
     [java] B(0) = 1
     [java] B(1) = -1/2
     [java] B(2) = 1/6
     [java] B(3) = 0

BUILD SUCCESSFUL
```