package serverchannel.src;

import data.Bernoulli;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;
import java.nio.ByteBuffer;
import java.util.Scanner;

public class Client {

    public static void main(String[] args) {
        String host = "localhost";
        int port = 7999;
        if (args.length > 0) {
            host = args[0];
        }
        if (args.length > 1) {
            port = Integer.parseInt(args[1]);
        }

        Scanner scanner = new Scanner(System.in);
//        System.out.print("Introduceti n: ");
        int n = scanner.nextInt();

        SocketChannel socketChannel = null;

        try {
            InetSocketAddress isa = new InetSocketAddress(host, port);
            socketChannel = SocketChannel.open();
            socketChannel.connect(isa);
        }
        catch (Exception e) {
            System.err.println(e.getMessage());
            System.exit(1);
        }

        try {
            ByteBuffer bb = ByteBuffer.allocate(2048);
            bb.putInt(0, n);
            socketChannel.write(bb);
            bb.clear();
            socketChannel.read(bb);
            byte[] bin = bb.array();
            ByteArrayInputStream bais = new ByteArrayInputStream(bin);
            ObjectInputStream in = new ObjectInputStream(bais);
            Bernoulli bernoulli = (Bernoulli)in.readObject();
            bernoulli.print();
            socketChannel.close();
        }
        catch(Exception e) {
            System.err.println("Client communication error: "+e.getMessage());
        }
    }
}
