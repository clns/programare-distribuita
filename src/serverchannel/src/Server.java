package serverchannel.src;

import data.Bernoulli;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.ByteBuffer;
import java.io.IOException;

public class Server {

    public static void main(String[] args) {
        int port = 7999;
        if (args.length > 0) {
            port = Integer.parseInt(args[0]);
        }
        Server server = new Server();
        ServerSocketChannel serverSocketChannel = server.getServerSocketChannel(port);
        server.myAction(serverSocketChannel);
    }

    public ServerSocketChannel getServerSocketChannel ( int port ){
        ServerSocketChannel serverSocketChannel=null ;
        try{
            serverSocketChannel = ServerSocketChannel.open();
            InetSocketAddress isa = new InetSocketAddress(port);
            ServerSocket ss = serverSocketChannel.socket();
            ss.bind(isa);
        }
        catch( IOException e) {
            System.out.println("ServerSocketChannel error: "+e.getMessage());
            System.exit(1);
        }
        System.out.println("Server ready...");
        return serverSocketChannel;
    }

    public void myAction(ServerSocketChannel serverSocketChannel) {
        while(true) {
            try(SocketChannel socketChannel = serverSocketChannel.accept()) {
                ByteBuffer bb = ByteBuffer.allocate(2048);
                socketChannel.read(bb);
                int n = bb.getInt(0);
                Bernoulli bernoulli = new Bernoulli(n);

                ByteArrayOutputStream baos = new ByteArrayOutputStream(256);
                ObjectOutputStream out = new ObjectOutputStream(baos);
                out.writeObject(bernoulli);
                byte[] bout = baos.toByteArray();
                bb = ByteBuffer.wrap(bout);
                socketChannel.write(bb);
                socketChannel.close();
            }
            catch(Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

}
