package applet.src;

import data.Bernoulli;

import java.awt.*;
import java.applet.*;
import java.awt.event.*;

public class VisualBernoulli extends Applet implements ActionListener {
    TextField tn;
    TextArea rez;

    public void init() {
        setBackground(Color.yellow);
        GridLayout gl = new GridLayout(3, 2, 30, 20);
        setLayout(gl);
        Label ln = new Label("N:");
        add(ln);
        tn = new TextField("3", 1);
        add(tn);
        Button compute = new Button("Generate");
        compute.addActionListener(this);
        add(compute);
        rez = new TextArea(10, 10);
        add(rez);
    }

    public void paint(Graphics g) {
        String sn = tn.getText();
        int n = Integer.parseInt(sn);
        Bernoulli bernoulli = new Bernoulli(n);
        rez.setText(bernoulli.asString());
    }

    public void actionPerformed(ActionEvent ae) {
        repaint();
    }


}