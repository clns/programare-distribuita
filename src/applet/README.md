## Applet

### Exemplu Executie

#### Applet

```
Calins-MacBook-Pro:Bernoulli calin$ bin/applet.sh 
Buildfile: /Users/calin/IdeaProjects/Bernoulli/src/applet/build.xml

Init:
   [delete] Deleting directory /Users/calin/IdeaProjects/Bernoulli/src/applet/public
    [mkdir] Created dir: /Users/calin/IdeaProjects/Bernoulli/src/applet/public

Compile:
    [javac] Compiling 1 source file to /Users/calin/IdeaProjects/Bernoulli/src/applet/public
      [jar] Building jar: /Users/calin/IdeaProjects/Bernoulli/src/applet/public/visual-bernoulli.jar

Certificate:
     [exec] 
     [exec] Warning: 
     [exec] The signer certificate will expire within six months.

Deploy:
     [copy] Copying 1 file to /Library/WebServer/Documents/progr-distr/applet

BUILD SUCCESSFUL
```

#### Client

Se acceseaza http://localhost/progr-distr/applet/ in browser.

![applet.png](https://bitbucket.org/repo/oAjoMy/images/810442867-applet.png)