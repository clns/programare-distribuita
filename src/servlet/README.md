## Servlet

### Exemplu Executie

#### Server

* Pornire apache-tomcat

```
Calins-MacBook-Pro:Bernoulli calin$ bin/apachetomcat-start.sh
Using CATALINA_BASE:   /Users/calin/Downloads/apache-tomcat-8.0.8
Using CATALINA_HOME:   /Users/calin/Downloads/apache-tomcat-8.0.8
Using CATALINA_TMPDIR: /Users/calin/Downloads/apache-tomcat-8.0.8/temp
Using JRE_HOME:        /Library/Java/JavaVirtualMachines/jdk1.7.0_45.jdk/Contents/Home
Using CLASSPATH:       /Users/calin/Downloads/apache-tomcat-8.0.8/bin/bootstrap.jar:/Users/calin/Downloads/apache-tomcat-8.0.8/bin/tomcat-juli.jar
Tomcat started.
```

* Generare servlet

```
Calins-MacBook-Pro:Bernoulli calin$ bin/servlet.sh
Buildfile: /Users/calin/IdeaProjects/Bernoulli/src/servlet/build.xml

Init:
   [delete] Deleting directory /Users/calin/IdeaProjects/Bernoulli/src/servlet/web/WEB-INF/classes
    [mkdir] Created dir: /Users/calin/IdeaProjects/Bernoulli/src/servlet/web/WEB-INF/classes
   [delete] Deleting directory /Users/calin/IdeaProjects/Bernoulli/src/servlet/web/dist
    [mkdir] Created dir: /Users/calin/IdeaProjects/Bernoulli/src/servlet/web/dist
   [delete] Deleting directory /Users/calin/Downloads/apache-tomcat-8.0.8/webapps/bernoulli-servlet
    [mkdir] Created dir: /Users/calin/Downloads/apache-tomcat-8.0.8/webapps/bernoulli-servlet

Compile:
    [javac] Compiling 1 source file to /Users/calin/IdeaProjects/Bernoulli/src/servlet/web/WEB-INF/classes

Generate:
      [jar] Building jar: /Users/calin/IdeaProjects/Bernoulli/src/servlet/web/dist/bernoulli-servlet.war

Deploy:
     [copy] Copying 6 files to /Users/calin/Downloads/apache-tomcat-8.0.8/webapps/bernoulli-servlet
     [copy] Copied 7 empty directories to 1 empty directory under /Users/calin/Downloads/apache-tomcat-8.0.8/webapps/bernoulli-servlet

BUILD SUCCESSFUL
```

#### Client

Se acceseaza http://localhost:8080/bernoulli-servlet/ in browser.

![servet-1.png](https://bitbucket.org/repo/oAjoMy/images/1506597967-servet-1.png)

![servet-2.png](https://bitbucket.org/repo/oAjoMy/images/1216712705-servet-2.png)