package servlet.src;

import data.Bernoulli;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import javax.servlet.annotation.WebServlet;

//@WebServlet(urlPatterns = "/bernoulli")
public class Servlet extends HttpServlet {
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String sn = req.getParameter("n");
        String tip = req.getParameter("tip");
        int n = Integer.parseInt(sn);
        Bernoulli bernoulli = new Bernoulli(n);
        PrintWriter out = res.getWriter();
        if (tip.equals("text/html")) {
            String title = "Bernoulli Servlet";
            res.setContentType("text/html");
            out.println("<!DOCTYPE HTML><html><head><title>");
            out.println(title);
            out.println("</title></head><body>");
            out.println("<h1>"+title+"</h1>");
            out.println("<p>Bernoulli for N="+n+" is:<br>"+bernoulli.asHtml()+"</p>");
            out.println("</body></html>");
        } else {
            res.setContentType("text/plain");
            out.println(bernoulli.asString());
        }
        out.close();
    }
    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        doGet(req, res);
    }
}