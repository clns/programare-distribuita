package tcp.src;

import data.Bernoulli;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    public static void main(String[] args) {
        String host = "localhost";
        int port = 7999;
        if (args.length > 0) {
            host = args[0];
        }
        if (args.length > 1) {
            port = Integer.parseInt(args[1]);
        }

        Scanner scanner = new Scanner(System.in);
//        System.out.print("Introduceti n: ");
        int n = scanner.nextInt();

        try(Socket socket = new Socket(host, port);
            ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
            ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream())
        ) {
            out.writeInt(n);
            out.flush();

            Bernoulli bernoulli = (Bernoulli)in.readObject();
            bernoulli.print();
            socket.close();
        }
        catch(Exception e){
            System.err.println("Client communication error: "+e.getMessage());
        }
    }
}
