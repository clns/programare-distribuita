package tcp.src;

import data.Bernoulli;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    public static void main(String[] args) {
        int port = 7999;
        if (args.length > 0) {
            port = Integer.parseInt(args[0]);
        }
        Server server = new Server();
        ServerSocket serverSocket = server.getServerSocket(port);
        server.myAction(serverSocket);
    }

    public ServerSocket getServerSocket(int port) {
        ServerSocket serverSocket = null;
        try{
            serverSocket = new ServerSocket(port);
        }
        catch (IOException e) {
            System.err.println("Could not listen on port: "+port);
            System.err.println(e.getMessage());
            System.exit(1);
        }
        System.out.println("ServerSocket is ready ...");
        return serverSocket;
    }

    public void myAction ( ServerSocket serverSocket ){
        while(true){
            try(Socket socket = serverSocket.accept();
                ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
                ObjectInputStream in = new ObjectInputStream(socket.getInputStream())
            ) {
                int n = in.readInt();
                Bernoulli bernoulli = new Bernoulli(n);
                out.writeObject(bernoulli);
                socket.close();
            }
            catch(IOException e) {
                System.err.println("Server communication error: "+e.getMessage());
            }
        }
    }
}
