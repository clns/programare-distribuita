## ServerSocket (TCP)

### Exemplu Executie

#### Server

```
Calins-MacBook-Pro:Bernoulli calin$ bin/tcp-server.sh

Buildfile: /Users/calin/IdeaProjects/Bernoulli/src/tcp/server.xml

Init:
   [delete] Deleting directory /Users/calin/IdeaProjects/Bernoulli/src/tcp/server
    [mkdir] Created dir: /Users/calin/IdeaProjects/Bernoulli/src/tcp/server

Compile:
    [javac] Compiling 1 source file to /Users/calin/IdeaProjects/Bernoulli/src/tcp/server

Run:
     [java] ServerSocket is ready ...
```

#### Client

* n=4

```
Calins-MacBook-Pro:Bernoulli calin$ bin/tcp-client.sh
Buildfile: /Users/calin/IdeaProjects/Bernoulli/src/tcp/client.xml

Init:
   [delete] Deleting directory /Users/calin/IdeaProjects/Bernoulli/src/tcp/client
    [mkdir] Created dir: /Users/calin/IdeaProjects/Bernoulli/src/tcp/client

Compile:
    [javac] Compiling 1 source file to /Users/calin/IdeaProjects/Bernoulli/src/tcp/client

Run:
4
     [java] B(0) = 1
     [java] B(1) = -1/2
     [java] B(2) = 1/6
     [java] B(3) = 0

BUILD SUCCESSFUL
```

* n=5

```
Calins-MacBook-Pro:Bernoulli calin$ bin/tcp-client.sh
Buildfile: /Users/calin/IdeaProjects/Bernoulli/src/tcp/client.xml

Init:
   [delete] Deleting directory /Users/calin/IdeaProjects/Bernoulli/src/tcp/client
    [mkdir] Created dir: /Users/calin/IdeaProjects/Bernoulli/src/tcp/client

Compile:
    [javac] Compiling 1 source file to /Users/calin/IdeaProjects/Bernoulli/src/tcp/client

Run:
5
     [java] B(0) = 1
     [java] B(1) = -1/2
     [java] B(2) = 1/6
     [java] B(3) = 0
     [java] B(4) = -1/30

BUILD SUCCESSFUL
```