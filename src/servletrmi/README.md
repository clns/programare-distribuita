## Servlet RMI

### Exemplu Executie

#### Server

* Server FTP

```
Calins-MacBook-Pro:Bernoulli calin$ bin/ftpserver.sh
Using XML configuration file res/conf/ftpd-typical.xml...
FtpServer started
```

* rmiregistry

```
Calins-MacBook-Pro:Bernoulli calin$ bin/servletrmi-registry.sh
Buildfile: /Users/calin/IdeaProjects/Bernoulli/src/rmi/server.xml

Rmi:
```

* Server RMI

```
Calins-MacBook-Pro:Bernoulli calin$ bin/servletrmi-server.sh
Buildfile: /Users/calin/IdeaProjects/Bernoulli/src/rmi/interface.xml

Init:
    [mkdir] Created dir: /Users/calin/IdeaProjects/Bernoulli/src/rmi/public

Compile:
    [javac] Compiling 1 source file to /Users/calin/IdeaProjects/Bernoulli/src/rmi/public
      [jar] Building jar: /Users/calin/IdeaProjects/Bernoulli/src/rmi/public/bernoulli.jar

BUILD SUCCESSFUL
Total time: 1 second
Buildfile: /Users/calin/IdeaProjects/Bernoulli/src/rmi/server.xml

Init:
    [mkdir] Created dir: /Users/calin/IdeaProjects/Bernoulli/src/rmi/server

Compile:
    [javac] Compiling 1 source file to /Users/calin/IdeaProjects/Bernoulli/src/rmi/server
    [unjar] Expanding: /Users/calin/IdeaProjects/Bernoulli/src/rmi/public/bernoulli.jar into /Users/calin/IdeaProjects/Bernoulli/src/rmi/server

Archive:
      [jar] Building jar: /Users/calin/Downloads/apache-ftpserver-1.0.6/res/home/rmi/bernoulli.jar

Server:
     [java] Bernoulli RMI server ready ...
```

* Generare servlet

```
Calins-MacBook-Pro:Bernoulli calin$ bin/servletrmi-client.sh
Buildfile: /Users/calin/IdeaProjects/Bernoulli/src/servletrmi/build.xml

Init:
   [delete] Deleting directory /Users/calin/IdeaProjects/Bernoulli/src/servletrmi/web/WEB-INF/classes
    [mkdir] Created dir: /Users/calin/IdeaProjects/Bernoulli/src/servletrmi/web/WEB-INF/classes
   [delete] Deleting directory /Users/calin/IdeaProjects/Bernoulli/src/servletrmi/web/dist
    [mkdir] Created dir: /Users/calin/IdeaProjects/Bernoulli/src/servletrmi/web/dist
   [delete] Deleting directory /Users/calin/Downloads/apache-tomcat-8.0.8/webapps/bernoulli-servletrmi
    [mkdir] Created dir: /Users/calin/Downloads/apache-tomcat-8.0.8/webapps/bernoulli-servletrmi

Compile:
     [copy] Copying 1 file to /Users/calin/IdeaProjects/Bernoulli/src/servletrmi/web/WEB-INF/lib
    [javac] Compiling 1 source file to /Users/calin/IdeaProjects/Bernoulli/src/servletrmi/web/WEB-INF/classes

Generate:
      [jar] Building jar: /Users/calin/IdeaProjects/Bernoulli/src/servletrmi/web/dist/bernoulli-servletrmi.war

Deploy:
     [copy] Copying 5 files to /Users/calin/Downloads/apache-tomcat-8.0.8/webapps/bernoulli-servletrmi

BUILD SUCCESSFUL
```

#### Client

Se acceseaza http://localhost:8080/bernoulli-servletrmi/ in browser.

![servlet-rmi-1.png](https://bitbucket.org/repo/oAjoMy/images/3086106959-servlet-rmi-1.png)

![servet-rmi-2.png](https://bitbucket.org/repo/oAjoMy/images/3329427744-servet-rmi-2.png)