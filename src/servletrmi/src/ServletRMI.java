package servletrmi.src;

import java.io.*;
import java.rmi.*;
import java.rmi.registry.*;
import javax.servlet.*;
import javax.servlet.http.*;
import rmi.src.Interface;
import data.Bernoulli;
//import javax.servlet.annotation.WebServlet;

//@WebServlet(urlPatterns = "/bernoulli")
public class ServletRMI extends HttpServlet {
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String sn = req.getParameter("n");
        String tip = req.getParameter("tip");
        String host = req.getParameter("host").trim();
        String sPort = req.getParameter("port");
        int port = Integer.parseInt(sPort);
        int n = Integer.parseInt(sn);
        Bernoulli bernoulli = null;
        PrintWriter out = res.getWriter();

        try {
            Interface obj = (Interface)Naming.lookup("//"+host+":"+port+"/Bernoulli");
            bernoulli = obj.bernoulli(n);
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }

        if (tip.equals("text/html")) {
            String title = "Bernoulli Servlet RMI";
            res.setContentType("text/html");
            out.println("<!DOCTYPE HTML><html><head><title>");
            out.println(title);
            out.println("</title></head><body>");
            out.println("<h1>"+title+"</h1>");
            out.println("<p>Bernoulli for N="+n+" is:<br>"+bernoulli.asHtml()+"</p>");
            out.println("</body></html>");
        } else {
            res.setContentType("text/plain");
            out.println(bernoulli.asString());
        }
        out.close();
    }
    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        doGet(req, res);
    }
}