package data;
import java.math.BigInteger;
import java.io.Serializable;

/**
 * Bernoulli numbers
 *
 * @author calin
 */
public class Bernoulli implements Serializable {

    int n = 0;
    Rational[] bernoulli = null;

    public Bernoulli(int n) {

        this.n = n;

        // precompute binomial coefficients
        BigInteger[][] binomial = new BigInteger[n+1][n+1];
        for (int j = 1; j <= n; j++) binomial[0][j] = BigInteger.ZERO;
        for (int i = 0; i <= n; i++) binomial[i][0] = BigInteger.ONE;

        // bottom-up dynamic programming
        for (int i = 1; i <= n; i++)
            for (int j = 1; j <= n; j++)
                binomial[i][j] = binomial[i-1][j-1].add(binomial[i-1][j]);

        // now compute Bernoulli numbers
        bernoulli = new Rational[n+1];
        bernoulli[0] = new Rational(1, 1);
        bernoulli[1] = new Rational(-1, 2);
        for (int k = 2; k < n; k++) {
            bernoulli[k] = new Rational(0, 1);
            for (int i = 0; i < k; i++) {
                Rational coef = new Rational(binomial[k + 1][k + 1 - i], BigInteger.ONE);
                bernoulli[k] = bernoulli[k].minus(coef.times(bernoulli[i]));
            }
            bernoulli[k] = bernoulli[k].divides(new Rational(k+1, 1));
        }
    }

    public void print() {
        for (int i = 0; i < this.n; i++) {
            System.out.println("B(" + i + ") = " + bernoulli[i]);
        }
    }

    public String asString() {
        return this.asString("\n");
    }

    public String asHtml() {
        return this.asString("<br>");
    }

    private String asString(String separator) {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < this.n; i++) {
            s.append("B(").append(i).append(") = ").append(bernoulli[i]).append(separator);
        }
        return s.toString();
    }
}