package data;
import java.math.BigInteger;
import java.io.Serializable;

/**
 * Class for representing arbitrarily large rational numbers
 *
 * @author calin
 */
public class Rational implements Comparable<Rational>, Serializable {

    public final static Rational ZERO = new Rational(0);

    private BigInteger num;   // the numerator
    private BigInteger den;   // the denominator


    // create and initialize a new Rational object
    public Rational(int numerator, int denominator) {
        this(new BigInteger("" + numerator), new BigInteger("" + denominator));
    }

    // create and initialize a new Rational object
    public Rational(int numerator) {
        this(numerator, 1);
    }

    // create and initialize a new Rational object from a string, e.g., "-343/1273"
    public Rational(String s) {
        String[] tokens = s.split("/");
        if (tokens.length == 2)
            init(new BigInteger(tokens[0]), new BigInteger(tokens[1]));
        else if (tokens.length == 1)
            init(new BigInteger(tokens[0]), BigInteger.ONE);
        else
            throw new RuntimeException("Parse error in Rational");
    }

    // create and initialize a new Rational object
    public Rational(BigInteger numerator, BigInteger denominator) {
        init(numerator, denominator);
    }

    private void init(BigInteger numerator, BigInteger denominator) {

        // deal with x / 0
        if (denominator.equals(BigInteger.ZERO)) {
            throw new RuntimeException("Denominator is zero");
        }

        // reduce fraction
        BigInteger g = numerator.gcd(denominator);
        num = numerator.divide(g);
        den = denominator.divide(g);

        // to ensure invariant that denominator is positive
        if (den.compareTo(BigInteger.ZERO) < 0) {
            den = den.negate();
            num = num.negate();
        }
    }

    // return string representation of (this)
    public String toString() {
        if (den.equals(BigInteger.ONE)) return num + "";
        else                            return num + "/" + den;
    }

    // return { -1, 0, + 1 } if a < b, a = b, or a > b
    public int compareTo(Rational b) {
        Rational a = this;
        return a.num.multiply(b.den).compareTo(a.den.multiply(b.num));
    }

    // is this Rational negative, zero, or positive?
    public boolean isZero()     { return compareTo(ZERO) == 0; }
    public boolean isPositive() { return compareTo(ZERO)  > 0; }
    public boolean isNegative() { return compareTo(ZERO)  < 0; }

    // is this Rational object equal to y?
    public boolean equals(Object y) {
        if (y == this) return true;
        if (y == null) return false;
        if (y.getClass() != this.getClass()) return false;
        Rational b = (Rational) y;
        return compareTo(b) == 0;
    }

    // hashCode consistent with equals() and compareTo()
    public int hashCode() {
        return this.toString().hashCode();
    }


    // return a * b
    public Rational times(Rational b) {
        Rational a = this;
        return new Rational(a.num.multiply(b.num), a.den.multiply(b.den));
    }

    // return a + b
    public Rational plus(Rational b) {
        Rational a = this;
        BigInteger numerator   = a.num.multiply(b.den).add(b.num.multiply(a.den));
        BigInteger denominator = a.den.multiply(b.den);
        return new Rational(numerator, denominator);
    }

    // return -a
    public Rational negate() {
        return new Rational(num.negate(), den);
    }

    // return a - b
    public Rational minus(Rational b) {
        Rational a = this;
        return a.plus(b.negate());
    }

    // return 1 / a
    public Rational reciprocal() {
        return new Rational(den, num);
    }

    // return a / b
    public Rational divides(Rational b) {
        Rational a = this;
        return a.times(b.reciprocal());
    }

}