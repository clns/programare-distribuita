#!/bin/bash

# 1
# Make sure apachetomcat-start, ftpserver, servletrmi-registry and servletrmi-server are started in different terminals

# 2
ant -buildfile src/servletrmi/build.xml

# 3
# Open the browser at http://localhost:8080/bernoulli-servletrmi/ to see the result